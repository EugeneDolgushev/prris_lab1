package com.company;

public class Distance {

    public static final int NO_RESULT = -1;

    private Integer minDistance = Integer.MAX_VALUE, maxDistance = 0;

    public Integer getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public Integer getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }
}
