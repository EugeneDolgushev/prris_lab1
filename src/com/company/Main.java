package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Main {

    private static final String INPUT_FILE_NAME = "input.txt";
    private static String firstWord, secondWord;
    private static HashSet<String> signs = new HashSet<>(Arrays.asList("!", "\"", ";", ":", "?", ".", ",", "'", " "));
    private static List<Integer> firstWordIndexes = new ArrayList<>(), secondWordIndexes = new ArrayList<>();
    private static Distance distance = new Distance();

    public static void main(String[] args) {
        try {
            readWordsFromInput();
            readFile();
            findMaxDistance();
            writeResult(distance);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFile() throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(INPUT_FILE_NAME));
        int min = Integer.MAX_VALUE;
        int minLastPosWord1 = -1;
        int minLastPosWord2 = -1;
        String line;
        int globalIndex = 0;
        while ((line = fileReader.readLine()) != null) {
            String[] words = line.split(" ");
            for (int i = 0; i < words.length; ++i, ++globalIndex) {
                if (firstWord.equals(cropFromLastSign(words[i]))) {
                    firstWordIndexes.add(globalIndex);
                    minLastPosWord1 = globalIndex;
                    int minDistance = Math.abs(minLastPosWord1 - minLastPosWord2) - 1;
                    if (minLastPosWord2 >= 0 && min > minDistance) {
                        min = minDistance;
                    }
                } else if (secondWord.equals(cropFromLastSign(words[i]))) {
                    secondWordIndexes.add(globalIndex);
                    minLastPosWord2 = globalIndex;
                    int minDistance = Math.abs(minLastPosWord2 - minLastPosWord1) - 1;
                    if (minLastPosWord1 >= 0 && min > minDistance) {
                        min = minDistance;
                    }
                }
            }
        }
        fileReader.close();
        distance.setMinDistance(min);
    }

    private static void readWordsFromInput() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите первое слово: ");
        firstWord = bufferedReader.readLine();
        System.out.println("Введите второе слово: ");
        secondWord = bufferedReader.readLine();
    }

    private static void writeResult(Distance distance) {
        if (distance.getMinDistance() == Distance.NO_RESULT) {
            System.out.println("Min distance: NO_RESULT");
            System.out.println("Max distance: NO_RESULT");
        } else {
            System.out.println("Min distance: " + distance.getMinDistance());
            System.out.println("Max distance: " + distance.getMaxDistance());
        }
    }

    private static String cropFromLastSign(String word) {
        if (signs.contains(word.substring(word.length() - 1))) {
            return word.substring(0, word.length() - 1);
        } else {
            return word;
        }
    }

    private static void findMaxDistance() {
        if (firstWordIndexes.isEmpty() || secondWordIndexes.isEmpty()) {
            return;
        }

        int firstMaxDistance = Math.abs(firstWordIndexes.get(0) - secondWordIndexes.get(secondWordIndexes.size() - 1)) - 1;
        int secondMaxDistance = Math.abs(firstWordIndexes.get(firstWordIndexes.size() - 1) - secondWordIndexes.get(0)) - 1;
        if (firstMaxDistance > secondMaxDistance) {
            distance.setMaxDistance(firstMaxDistance);
        } else {
            distance.setMaxDistance(secondMaxDistance);
        }
    }
}
